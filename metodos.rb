# Variables globales y comunes
var = 'algo'
$var = 3

def saludo(nombre)
    # Variable local
    # var = 'esto'
    puts 'Hola ' + nombre
end

# Operaciones con Metodos
def suma(m, n)
    var = m + n
    puts var
end

def resta(m, n)
    var = m - n
    puts var
end

def division(m, n)
    var = m / n
    return var
    # puts var
end

puts 'Metodos con parametros'
saludo('Jose')
puts 'Operaciones con Metodos'
puts 'Suma'
suma(4, 7)
puts 'Resta'
resta(22, 77)
puts 'Division'
puts division(22, 4)
puts 'Redefinir metodos con alias'

alias divi division
puts divi(3, 4)

# Argumentos Variables dentro de un metodo se utilizan poniendo un asterisco *
# y crea un arreglo el cual puedo utilizar accediendo con corchetes
# y poniendo entre los corchetes el numero del elemento al que quiero acceder
def nombres(*args)
    # Se pueden pasar varios argumentos, variables, etc en un solo puts separandolos por una coma 
    puts args[4]
end

nombres('Angy', 'Jose', 'Luis', 'Daniel', 'Lupe')

# Argumentos Opcionales dentro de un metodo se utilizan poniendo un asterisco *
# pero se pueden o no usar y deben de ir al ultimo de todos los argumentos pasados
# al metodo
                  # ↓ argumento opcional que va al ultimo
def nombres(arg, *args) 
    # Se pueden pasar varios argumentos, variables, etc en un solo puts separandolos por una coma 
    puts args
end
        # ↓ El primero es obligatorio los demas son opcionales
nombres('Angy', 'Jose', 'Luis', 'Daniel', 'Lupe')