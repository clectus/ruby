# Con la clase time obtenemos el tiempo fecha del sistema
# Formatos de strftime
# %d formato de conteo del numero del dia
# %m formato de conteo del numero del mes
# %Y formato de conteo del numero del año

# %H formato de conteo de las horas
# %M formato de conteo de los minutos
# %Z formato de conteo de la zona horaria

# %A formato de conteo del nombre de los dias
# %B formato de conteo del nombre de los meses

tiempo = Time.now
puts tiempo.strftime("Hoy es %d del %m del %Y")
puts tiempo.strftime("Son las %H:%M %Z")
puts tiempo.strftime("Hoy es el dia %A del mes %B")
