# Creando una clase con parametros y metodos
class Auto

    # Metodo constructor, inicializador o de instancia
    def initialize(color, marca) # <-- Parametros
        # Atributos de la clase o variables del objeto
        @color = color  
        @marca = marca  
    end

    # Metodos o funciones, arranque y apagar
    def arranque
        # Variable comun
        var = 5
        # Variable de clase solo se utilizan dentro de la clase
        @@num = 4
        # Asi accedemos a las variables de objeto
        #            ↓                  ↓
        puts "El #{@marca} de color #{@color} se ha encendido"
    end

    def apagar
        puts "El #{@marca} de color #{@color} se ha apagado"
    end

    # Accesores habilitan nuestros atributos de la clase
    # para poder ser accedidos desde el objeto

    # Accesor de lectura
    # attr_reader   :color, :marca
    # Accesor de sobrescritura
    # attr_writer   :color, :marca
    # Accesor de sobrescritura y lectura
    attr_accessor :color, :marca
end

# Creamos la instancia del objeto
coche = Auto.new('Morado', 'Audi')
coche2 = Auto.new('Verde', 'Hunter')
# Accedemos a los metodos de la clase
coche.arranque
coche.apagar
coche2.arranque
coche2.apagar
# Nos muestra que coche pertenece a la clase Auto
#            ↓
puts coche.class
# Preguntamos a que clase pertenece el objeto
#                 ↓ Devuelve true o false
puts (coche.instance_of? Auto)

# Obtener el identificador del objeto
#               ↓ 
puts coche.object_id
# *****************************************************
puts "El identificador del objeto coche es #{coche.object_id}"
# ******************************************************
# Aqui estamos cambiando las propiedades de los atributos
# gracias a los accesores
coche2.marca = 'Vocho'
coche2.color = 'Rojo'
# ****************************************************
puts coche2.marca
puts coche2.color
