# Operadores logicos
# < menor que
# > mayor que
# <= menor o igual que
# >= mayor o igual que
# == igual

# valor = 11 == 22
# puts valor

# if false
#     puts 'Es verdadero'  
# else
#     puts 'Es falso'
# end

# < menor que
if 3 < 8
    puts 'Es verdadero'
elsif 9 < 4
    puts 'Es falso'
end

puts ''
# Condicion unless valida si es falso muestra el resultado
# de ser verdadero no muestra nada
unless 4 == 3
    puts 'Es falso'
end

# Condicion case valida una condicion
# Si se cumple muestra el texto
# Sino no muestra nada
valor = 4

case valor
when 1
    puts 'Vale 1'
when 2
    puts 'Vale 2'
when 3
    puts 'Vale 3'
end

# Condicion case con rango valida una condicion igual que la sin rango
# Si se cumple muestra el texto
# Sino no muestra nada
val = 22

case val
when 0..10
    puts 'El valor esta entre 0 y 10'
when 11..20
    puts 'El valor esta entre 11 y 20'
when 21..30
    puts 'El valor esta entre 21 y 30'
when 31..40
    puts 'El valor esta entre 31 y 40'
end