# Sobrecarga de métodos es cuando tenemos 2 metodos iguales
# pero a uno de ellos le pasamos parametros

class Vehiculo

    def initialize(marca, color)
        @marca = marca
        @color = color
    end

    def arranque
        puts "El #{@marca} de color #{@color} se ha encendido"
    end

    def apagar
        puts "El #{@marca} de color #{@color} se ha apagado"
    end

    # Sobrecargamos el metodo pasandole parametros
    def apagar(seg)
        puts "El #{@marca} de color #{@color} se ha apagado en #{seg} segundos"
    end

    attr_accessor :marca, :color
    
end

class Auto < Vehiculo
# Esta clase hereda de la clase Vehiculo
# y por lo tanto la podemos instanciar para acceder
# a todos los metodos de la clase Vehiculo
end

coche = Auto.new('Audi', 'Verde')
puts coche.apagar(22)